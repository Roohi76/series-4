package sbu.cs.parser.json;

import javax.lang.model.type.NullType;
import java.util.ArrayList;
import java.util.List;

public class JsonParser
{
    // a list for storing objects of Data class
    public static List<Data> jsonKeys = new ArrayList<>();

    /*
    * this function will get a String and returns a Json object
     */
    public static Json parse(String data)
    {
        String[][] secondSplit = trim(data);
        for (int j = 0; j < secondSplit.length; j++)
        {
            if (secondSplit[j][1].matches("[a-zA-Z]+"))
            {
                Data<String> jsonKey = new Data<>();
                jsonKey.set(secondSplit[j][0], secondSplit[j][1]);
                jsonKeys.add(jsonKey);
            }
            else if (secondSplit[j][1].matches("\\d+"))
            {
                Data<Integer> jsonKey = new Data<>();
                jsonKey.set(secondSplit[j][0], Integer.valueOf(secondSplit[j][1]));
                jsonKeys.add(jsonKey);
            }
            else if (secondSplit[j][1].matches("\\d*[.]\\d+"))
            {
                Data<Double> jsonKey = new Data<>();
                jsonKey.set(secondSplit[j][0], Double.valueOf(secondSplit[j][1]));
                jsonKeys.add(jsonKey);
            }
            else if (secondSplit[j][1].matches("[t|T][r|R][u|U][e|E]"))
            {
                Data<Boolean> jsonKey = new Data<>();
                jsonKey.set(secondSplit[j][0], Boolean.TRUE);
                jsonKeys.add(jsonKey);
            }
            else if (secondSplit[j][1].matches("[f|F][a|A][l|L][s|S][e|E]"))
            {
                Data<Boolean> jsonKey = new Data<>();
                jsonKey.set(secondSplit[j][0], Boolean.FALSE);
                jsonKeys.add(jsonKey);
            }
            else if (secondSplit[j][1].matches("\\[.+]"))
            {
                secondSplit[j][1] = secondSplit[j][1].replaceAll("\\*", ", ");
                Data<String> jsonKey = new Data<>();
                jsonKey.set(secondSplit[j][0], secondSplit[j][1]);
                jsonKeys.add(jsonKey);
            }
            else
            {
                Data<NullType> jsonKey = new Data<>();
                jsonKey.set(secondSplit[j][0], null);
                jsonKeys.add(jsonKey);
            }
        }
        return new Json(jsonKeys);
    }

    /*
    * trim and split
    * i mean removing whitespaces and unnecessary characters
     */
    public static String[][] trim(String data)
    {
        data = data.replaceAll("(^\\{|}$|[\\s\"]*)", "");
        if (data.indexOf('[') >= 0)
        {
            String subData = data.substring(data.indexOf('['), data.indexOf(']') + 1);
            data = data.replaceAll("\\[.+?]", (subData.replaceAll(",", "*")));
        }
        String[] firstSplit = data.split(",");
        String[][] secondSplit = new String[firstSplit.length][2];
        for (int i = 0; i < firstSplit.length; i++)
        {
            secondSplit[i] = firstSplit[i].split(":");
        }
        return secondSplit;
    }

    /*
    * this method bundles with ArrayData class
    * it was meant to bring the code to perfection
    * but i didn't have enough time complete and fix it
     */
    /*public static void arrayParse (String key, String value)
    {
        value = value.replaceAll("^\\[|]$", "");
        value = value.replaceAll("]\\*\\[", "\\],\\[");
        String[] arrayValues = value.split("\\*");
        if (arrayValues[0].matches("[a-zA-Z]+"))
        {
            ArrayData<String> arrayKey = new ArrayData<>();
            arrayKey.setValue(key, arrayValues);
        }
        else if (arrayValues[0].matches("\\d+"))
        {
            Integer[] integers = new Integer[arrayValues.length];
            for (int i = 0; i < arrayValues.length; i++)
            {
                integers[i] = Integer.valueOf(arrayValues[i]);
            }
            ArrayData<Integer> arrayKey = new ArrayData<>();
            arrayKey.setValue(key, integers);
        }
        else if (arrayValues[0].matches("\\d*[.]\\d+"))
        {
            Double[] doubles = new Double[arrayValues.length];
            for (int i = 0; i < arrayValues.length; i++)
            {
                doubles[i] = Double.valueOf(arrayValues[i]);
            }
            ArrayData<Double> arrayKey = new ArrayData<>();
            arrayKey.setValue(key, doubles);
        }
        else if (arrayValues[0].matches("([t|T][r|R][u|U][e|E])|([f|F][a|A][l|L][s|S][e|E])"))
        {
            Boolean[] booleans = new Boolean[arrayValues.length];
            for (int i = 0; i < arrayValues.length; i++)
            {
                booleans[i] = Boolean.valueOf(arrayValues[i]);
            }
            ArrayData<Boolean> arrayKey = new ArrayData<>();
            arrayKey.setValue(key, booleans);
        }
        else
        {
            NullType[] nulls = new NullType[arrayValues.length];
            for (int i = 0; i < arrayValues.length; i++)
            {
                nulls[i] = null;
            }
            ArrayData<NullType> arrayKey = new ArrayData<>();
            arrayKey.setValue(key, nulls);
        }
    }*/

    /*
    * this function is the opposite of above function. implementing this has no score and
    * is only for those who want to practice more.
     */
    public static String toJsonString(Json data) {
        return null;
    }
}
