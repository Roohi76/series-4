package sbu.cs.parser.json;

import java.util.List;

public class Json implements JsonInterface
{
    private List<Data> data;

    public Json(List<Data> data)
    {
        this.data = data;
    }

    @Override
    public String getStringValue(String key)
    {
        for (int i = 0; i < data.size(); i++)
        {
            if (data.get(i).getKey().equals(key))
            {
                return String.valueOf(data.get(i).getValue());
            }
        }
        return null;
    }
}
