package sbu.cs.parser.json;

public class Data<T>
{
    private String key;
    private T value;

    public void set(String key, T value)
    {
        this.key = key;
        this.value = value;
    }

    public String getKey()
    {
        return key;
    }

    public T getValue()
    {
        return value;
    }
}
